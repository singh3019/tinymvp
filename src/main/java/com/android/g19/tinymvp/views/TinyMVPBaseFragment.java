package com.android.g19.tinymvp.views;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.g19.tinymvp.TinyMVPPresenter;
import com.android.g19.tinymvp.TinyMVPPresenterLoader;
import com.android.g19.tinymvp.TinyMVPView;

/**
 * Created by gagandeep on 27/7/16.
 */

public abstract class TinyMVPBaseFragment<VIEW extends TinyMVPView, PRESENTER extends TinyMVPPresenter<VIEW>> extends Fragment implements LoaderManager.LoaderCallbacks<PRESENTER> {

    private final int ID_PRESENTER_LOADER = Integer.MAX_VALUE - 100;
    private PRESENTER mPresenter;

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getActivity().getSupportLoaderManager().initLoader(ID_PRESENTER_LOADER, null, this);
    }

    @Override
    public Loader<PRESENTER> onCreateLoader(int id, Bundle args) {
        return new TinyMVPPresenterLoader<>(getContext(), createPresenter());
    }

    @Override
    public void onLoadFinished(Loader<PRESENTER> loader, PRESENTER presenter) {
        mPresenter = presenter;
        mPresenter.attachView( (VIEW) this);
        boolean persistedOne = presenter.isPersistedOne();
        if (!persistedOne) {
            presenter.setPersistedOne(true);
            presenter.onCreate();
        }
        onPresenterLoaded(presenter, persistedOne);
    }

    @Override
    public void onLoaderReset(Loader<PRESENTER> loader) {

    }

    protected abstract PRESENTER createPresenter();

    protected void onPresenterLoaded(PRESENTER presenter, boolean isPersistedOne) {
        if (!isPersistedOne) {
            presenter.onStart();
        }
    }

    protected PRESENTER getPresenter() {
        return mPresenter;
    }

    @Override
    public void onStart() {
        super.onStart();
        if (getPresenter() != null) {
            getPresenter().onStart();
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        getPresenter().onStop();
    }

    @Override
    public void onDetach() {
        getPresenter().detachView();
        super.onDetach();
    }

    @Override
    public void onDestroy() {
        getPresenter().onDestroy();
        super.onDestroy();
    }
}
